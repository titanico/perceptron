Introdução ao Aprendizado de Máquina em Python
----------------------------------------------

Introdução
-
O aprendizado de máquina (em inglês, machine learning) é um método de análise de dados. Ele automatiza a construção de modelos analíticos. É um ramo da inteligência artificial, e tem como conceito chave a capacidade do sistema de identificar padrões, tomar decisões e aprender com dados. No século 21, dados são recursos que temos em abundância.

Esta área não é nova. Porém, com o desenvolvimento tecnológico atual, os modelos se tornaram muito mais rápidos e adaptáveis. O aspecto iterativo de tais modelos os tornam extremamente importantes em uma era informatizada e que gera grandes volumes de dados diariamente. O aprendizado de máquina é muito usado em sistemas financeiros, análise de sentimentos, big data, automação automotiva e residencial, telecomunicação, etc. Aprender um pouco sobre essa crescente área é importante para qualquer futuro profissional na área de tecnologia.

Este tutorial visa ensinar de maneira prática a implementação e treinamento de um modelo Perceptron, um dos mais simples no ramo de aprendizado supervisionado. Alguns elementos teóricos foram omitidos para simplificar o entendimento.

Conceitos básicos
-

O aprendizado de máquina tem como objetivo desenvolver algoritmos de auto aprendizado para o desenvolvimento de modelos que possam fazer previsões ou análise de dados incompletos. Tais algoritmos são soluções elegantes, permitindo que o modelo faça suas próprias inferências e conclusões baseadas num conjunto de dados, excluindo o trabalho humano de duas derivações e análises. Modelos de aprendizado de máquina lidam muito bem com grandes volumes de dados,

O aprendizado supervisionado é um dos três tipos de aprendizado, sendo os outros dois o aprendizado não supervisionado e o aprendizado reforçado. Quando se fala “supervisionado”, refere-se ao fato do modelo receber um conjunto de dados completamente conhecido, chamado de conjunto de treinamento, onde cada entrada do algoritmo já tem sua saída conhecida. Por exemplo, é possível criar uma base de dados de e-mails classificados previamente como spam e não spam. A partir dele, um modelo pode ser treinado e capacitado a diferenciar e-mails que são ou não spams. Este tipo de tarefa é chamada de classificação, onde o modelo pode identificar o tipo de um conjunto de dados a partir de seus atributos.

Os atributos são um aspecto da instância, um elemento que retém uma informação importante para a análise do modelo. Por exemplo, uma pessoa poderia ter o peso, altura, e idade como atributos. Um atributo especial, chamado classe, é indispensável para o aprendizado supervisionado.

O tipo mais básico de modelo para tarefas de classificação é o perceptron, o neurônio artificial. Ele é a unidade mais básica da estrutura chamada rede neural. Assim como o neurônio biológico, o perceptron é capaz de emitir ou barrar sinais. Caso seu conjunto de entradas cumpra certas condições, ele irá emitir um sinal. Caso contrário, não irá fazer nada. Este comportamento nos permite fazer uma classificação binária (True ou False). Combinar vários perceptrons nos permite criar modelos complexos (multi-layered perceptron) e que permitem a identificação de mais classes.
![Um perceptron, sendo x1, x2 e x3 as entradas, w1, w2 e w3 os pesos. A saída é representada por y.](/Imgs/fig.png)

O funcionamento do perceptron é dependente da multiplicação das entradas por pesos. Os pesos permitem que cada entrada receba um grau de importância distinto. Durante o processo de treinamento, os pesos são atualizados a cada nova instância encontrada, a partir da previsão correta ou não feita pelo modelo. Por isso, é importante fornecer o maior número possível de dados para o modelo. Assim, seus pesos serão adaptados de maneira eficiente. Após o treinamento, o modelo estará apto a fazer boas previsões e classificações para dados que estejam fora do conjunto de treinamento.

Implementando um Perceptron
-

A implementação abaixo nos permite inicializar objetos perceptron.  Muitos conceitos dependem de orientação a objetos e lógica de programação, então não se preocupe caso não entenda tudo o que foi definido. Fique atento aos comentários no código para entender as funções mais importantes!

Utilize o código **perceptron.py** para fazer as atividades propostas no final do tutorial.

A classe perceptron possui dois métodos de importância para a análise inicial: fit e predict.

O método fit treina o modelo a partir do conjunto de dados de entrada, considerando zero como o valor inicial de todos os pesos. Depois, este método percorrerá cada instância do conjunto de dados, realizando suas inferências. A cada repetição, o perceptron irá comparar a saída que ele previu com a saída verdadeira. As previsão que não coincidir com a classe original irá desencadear uma correção nos pesos (aprendizado). Em caso de acerto, correções não são necessárias.

O predict gera as classificações a partir do aprendizado do modelo, e retornará a classe que o perceptron julgou como correta. Este método também é executado dentro do método fit.

Treinando um Perceptron
-
Treinaremos nosso perceptron a partir do conjunto de dado Iris Dataset, disponível no UCI Dataset Repository. Este conjunto de dados é utilizado em muitos outros tutoriais. Extrairemos os dados de flores com as classes Versicolor ou Setosa. É importante notar que as estruturas de dados só numPy são ligeiramente diferentes das originais em Python, portanto é bom prestar atenção em como cada lista o vetor é implementado e acessado.

Acesse o arquivo **exemplo.py** para ver a implementação.

Os seguintes pacotes foram utilizados no código:
- pandas: Permite carregar arquivos csv diretamente de Urls;
- numPy: estruturas de dados e matemática;
- matPlotLib: geração de gráficos.

O primeiro gráfico que surge após a execução do código é o da figura 1. Ele relaciona os dois atributos escolhidos com as classes da instâncias do conjunto de treinamento. Percebe-se que há uma clara distinção entre as duas classes, sem pontos comuns entre elas. Isso torna esta tarefa (pra os atributos escolhidos) linearmente divisível, ou seja, podemos traçar uma linha entre as instâncias e dividir perfeitamente o espaço das duas classes. Em caso contrário, precisaríamos de mais perceptrons (rede neural de múltipla camada) para conseguir treinar este modelo. 

![Mapeamento das instâncias pela classe e atributos.](/Imgs/Figure_1.png)

![Tarefa linearmente divisível.](/Imgs/Figure_1_1.png)

O segundo gráfico nos mostra a convergência do algoritmo de treinamento. Podemos ver que, conforme as épocas avançam, o modelo vai cometendo cada vez menos erros. É na época 6 que vemos que ele está fazendo as previsões corretamente, e portanto seu treinamento foi concluído naquele momento.

![Convergência fica evidente na época 6.](/Imgs/Figure_2.png)

A verificação da qualidade do treinamento é feita logo em seguida. Vemos que o modelo não comete erro algum, acertando todas as classificações.

### Atividades Propostas

As atividades abaixo requerem análise dos códigos de exemplo, pesquisa e experimentação. Caso não entenda algo, não hesite em perguntar!

1) Faça uma analise do conjunto de dados iris.data. Baixe o arquivo da url, e veja seu conteúdo. Utilizando os códigos anteriores como base, determine a quais são os atributos das instâncias, e quais as classes existentes. Faça um gráfico (com código) que relacione os atributos [petal length] e [sepal length] com cada classe presente.

2) Altere o código example.py para treinar o perceptron com atributos diferentes dos utilizados na implementação original. Quais foram os atributos escolhidos? O conjunto de dados é linearmente divisível? O resultado das previsões foi consistente como antes?

3) Acesse o conjunto de dados [https://archive.ics.uci.edu/ml/datasets/banknote+authentication]. Do que ele se trata? Quais são os atributos e classes presentes?

4) Para o conjunto de dados anterior, implemente um perceptron para a tarefa de classificação. Utilize todos os atributos como entrada, e experimente diferentes valores de [eta] e [n_iter]. Depois, elabore um relatório mostrando a convergência e o desempenho do perceptron.

Bibliografia:
- 
https://towardsdatascience.com/what-the-hell-is-perceptron-626217814f53?gi=93b004da35bf
https://archive.ics.uci.edu/ml/datasets/banknote+authentication
https://archive.ics.uci.edu/ml/machine-learning-databases/iris/
Livro Python Machine Learning, por Sebastian Raschka (tem no google drive da TCI)