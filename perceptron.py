import numpy as np

class Perceptron(object):
    """Perceptron classifier.
    Parâmetros
    ------------
    eta : float
    Taxa de aprendizado (entre 0.0 ae 1.0).
    n_iter : int
    Passadas (épocas) sobre o conjunto de treinamento.

    Attributos
    -----------
    w_ : 1d-array
    Vetor com pesos após o fit.
    errors_ : list
    Lista dúmero de classificações erradas em cada época.
    """
    def __init__(self, eta=0.01, n_iter=10):
        #Valores padrão eta = 0.01 e n_ter = 10
        #Podemos alterar na declaração do objeto!
        self.eta = eta
        self.n_iter = n_iter

    def fit(self, X, y):
        """Treinamento do perceptron
        Parâmetros
        ----------
        X : {array-like}, shape = [n_samples, n_features]
        Vetores de treinamento, onde n_sampes é o número de instâncias
        e n_features é o número de atributos.

        y : array-like, shape = [n_samples]
        Valores de saída.

        Retorna
        -------
        self : object
        """
        self.w_ = np.zeros(1 + X.shape[1])
        self.errors_ = []
        
        for _ in range(self.n_iter):
            errors = 0
            for xi, target in zip(X, y):
                update = self.eta * (target - self.predict(xi))

                self.w_[1:] += update * xi
                self.w_[0] += update
                errors += int(update != 0.0)
            self.errors_.append(errors)

        return self

    def net_input(self, X):
        """Calcula entrada da rede."""
        return np.dot(X, self.w_[1:]) + self.w_[0]
        
    def predict(self, X):
        """Retorna classe identificada pelo perceptron."""
        return np.where(self.net_input(X) >= 0.0, 1, -1)