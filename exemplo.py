from perceptron import Perceptron
import pandas
import matplotlib.pyplot as plot
from matplotlib.colors import ListedColormap
import numpy

#Carrega o dataset como um dataframe, a partir da URL.
data_frame = pandas.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data', header=None)

#Checan os 5 últimos valores para ver se os dados foram carregados.
data_frame.tail()

#Carrega as 100 primeiras classes (quarto índice), que são 50 Iris-Setosa e 50 Iris-Versicolor.
Y = data_frame.iloc[0:100, 4].values #Corta os 100 primeiros elementos, e pega o quinto valor de cada um deles. Coloca no dataframe.
Y = numpy.where(Y == 'Iris-setosa', -1, 1) #1 representa que é Iris-setosa, -1 em caso contrário.

#Carrega dois atributos para análise: sepal_length e petal_length. Os índices zero e quatro os acessam respectivamente.
X = data_frame.iloc[0:100, [0, 2]].values

#Constrói um gráfico que relaciona os dois atributos com a classe da instância.
plot.scatter(X[:50, 0], X[:50, 1], color='red', marker='*', label='setosa')
plot.scatter(X[50:100, 0], X[50:100, 1], color='blue', marker='x', label='versicolor')
plot.xlabel('sepal_length')
plot.ylabel('petal_length')
plot.legend(loc='upper left')
plot.show()

#Cria o perceptron. Utilizaremos os parâmetros com valores padrão.
perceptron = Perceptron(eta=0.1, n_iter=10)

#Treina o perceptron a partir dos X e Y declarados anteriormente.
perceptron.fit(X, Y)

#Constrói o gráfico que exibe quantos erros de classificação existiram em cada época de treinamento.
plot.plot(range(1, len(perceptron.errors_) + 1), perceptron.errors_,marker='o')
plot.xlabel('Épocas')
plot.ylabel('Número de class. incorretas')
plot.show()

#Testa-se se o perceptron é capaz de identificar cada classe a partir de seu aprendizado. Os dados foram escolhidos arbitráriamente entre os índices 0 e 100.
X1 = data_frame.iloc[0:25, [0, 2]].values
Y1 = numpy.where(data_frame.iloc[0:25, 4].values == 'Iris-setosa', -1, 1) #-1 é Iris-Setosa e 1 é Iris-Versicolor
X2 = data_frame.iloc[45:65, [0, 2]].values
Y2 = numpy.where(data_frame.iloc[45:65, 4].values == 'Iris-setosa', -1, 1) #-1 é Iris-Setosa e 1 é Iris-Versicolor

index = 0

#Legenda
#-1: Iris-Setonsa
#1: Iris-Versicolor
print("------ Teste 1 ------")
for instancia in X1:
    #Previsão do perceptron
    prev_classe = perceptron.predict(instancia)

    print("c. verdadeira = " + str(Y1[index]) + "," + " c. prevista = " + str(prev_classe) + ".")
    index += 1

index = 0
print("------ Teste 2 ------")
for instancia in X2:
    #Previsão do perceptron
    prev_classe = perceptron.predict(instancia)

    print("c. verdadeira = " + str(Y2[index]) + "," + " c. prevista = " + str(prev_classe) + ".")
    index += 1